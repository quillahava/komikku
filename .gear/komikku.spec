%define _unpackaged_files_terminate_build 1
%define xdg_name info.febvre.Komikku

Name: komikku
Version: 1.50.0
Release: alt1
Summary: A manga reader for GNOME
License: GPL-3.0-or-later
Group: Books/Other
Url: https://valos.gitlab.io/Komikku
Vcs: https://codeberg.org/valos/Komikku.git
BuildArch: noarch
Source0: %name-%version.tar

BuildRequires(pre): rpm-macros-meson
BuildRequires: meson
BuildRequires: libgtk4-devel
BuildRequires: blueprint-compiler
BuildRequires: python3(setuptools)
BuildRequires: pkgconfig(libadwaita-1)
BuildRequires: pkgconfig(gobject-introspection-1.0)

# Filter out gi.repository.GdkPixbuf dependency
%add_python3_req_skip gi.repository.GdkPixbuf

%description
Komikku is a manga reader for GNOME. It focuses on providing a clean,
intuitive and adaptive interface.

%prep
%setup

%build
%meson
%meson_build

%install
%meson_install
%find_lang --with-gnome %name
rm -fv %buildroot%_datadir/locale/zh_Hant/LC_MESSAGES/%name.mo

%check
%meson_test

%files -f %name.lang
%doc README.md CONTRIBUTING.md LICENSE
%_bindir/%name
%_datadir/%name/
%_desktopdir/%xdg_name.desktop
%python3_sitelibdir/%name/
%_iconsdir/hicolor/*/*/*.svg
%_datadir/glib-2.0/schemas/%xdg_name.gschema.xml
%_datadir/metainfo/%xdg_name.appdata.xml

%changelog
* Thu Jul 4 2024 Aleksandr A. Voyt <sobue@altlinux.org> 1.50.0-alt1
- Initial commit
