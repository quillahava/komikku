# Copyright (C) 2019-2024 Valéry Febvre
# SPDX-License-Identifier: GPL-3.0-only or GPL-3.0-or-later
# Author: Valéry Febvre <vfebvre@easter-eggs.com>

from komikku.servers.multi.madara import Madara


class Tecnoscan(Madara):
    id = 'tecnoscan'
    name = 'Tecno Scan'
    lang = 'es'
    status = 'disabled'  # dead 2024/06

    base_url = 'https://visortecno.com'
    chapters_url = base_url + '/manga/{0}/ajax/chapters/'
